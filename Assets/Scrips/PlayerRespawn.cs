using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRespawn : MonoBehaviour
{
    Vector3 _InitialPos; // Starting position of the player
    float _YOffset;
    void Start()
    {
        _InitialPos = transform.position;
        _YOffset = _InitialPos.y - 50;
    }

    void Update()
    {
        if (_InitialPos.y < _YOffset)
        {
            transform.position = _InitialPos;
        }
    }

}
