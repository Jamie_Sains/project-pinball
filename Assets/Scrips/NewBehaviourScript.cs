using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{

    public float speed;

    public float boostSpeed;



    private Rigidbody rb;

    private float movement;



    void Start()

    {

        rb = GetComponent<Rigidbody>();

    }

    //movement based off adding force to rigidbody.

    void FixedUpdate()

    {

        float moveHorizontal = Input.GetAxis("Horizontal");

        float moveVertical = Input.GetAxis("Vertical");



        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);



        rb.AddForce(movement * speed);

    }

    //Pick up trigger.

    void OnTriggerEnter(Collider other)

    {

        if (other.gameObject.CompareTag("Pick Up"))

        {

            other.gameObject.SetActive(false);

        }

        if (other.gameObject.CompareTag("Booster"))

        {

            Debug.Log("BOOST");



        }



    }
}

