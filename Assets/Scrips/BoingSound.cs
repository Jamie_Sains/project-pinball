using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoingSound : MonoBehaviour
{
    public AudioSource Boing;

    private void OnTriggerEnter(Collider other)
    {
        Boing.Play();
    }
}

