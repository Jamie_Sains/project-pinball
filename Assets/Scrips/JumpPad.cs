using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPad : MonoBehaviour
{
    public GameObject PlayerCtrl;
    public float AmountOfForce = 100000000000000;

    void OnTriggerEnter(Collider PlayerCtrl)
    {

        PlayerCtrl.transform.TransformDirection(Vector3.up * AmountOfForce);

    }
}
