using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoingSounds : MonoBehaviour
{
    public AudioSource Boing;

    void OnTriggerEnter()
    {
        Boing.Play();
    }

}
